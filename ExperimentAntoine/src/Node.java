import java.util.ArrayList;
import java.util.List;
class Node{
    public final double lonRadians;
    public final double latRadians;
    private ArrayList<Node> neighbors;
    private static double toRadians(double degrees){
        return degrees*Math.PI/180;
    }
    public Node(double lon, double lat){ //
        lonRadians= toRadians(lon);
        latRadians= toRadians(lat);
        neighbors= new ArrayList<Node>();
    }
    public void addNeighbor(Node other){
        neighbors.add(other);
    }
    public Node[] getNeighbors(){
        return neighbors.toArray(new Node[neighbors.size()]);
} }