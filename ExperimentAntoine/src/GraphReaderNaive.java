import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GraphReaderNaive {
// TODO !

	public static void main(String[] args) {
		long startTime = System.nanoTime();
		final String verticesFilename = "/Users/antoinemassy/Desktop/A2/Algorithmique et programation avancée/paris-vertices.txt";
		final String edgesFilename = "/Users/antoinemassy/Desktop/A2/Algorithmique et programation avancée/paris-edges.txt";
		try {
			Node[] nodes = readGraph("./data/paris-vertices.txt", "./data/paris-edges.txt");
			System.err.println("returned " + nodes.length + " elts.");
			Plotter p = new Plotter(nodes);
			p.plot(nodes);
		} catch (FileNotFoundException e) {
			System.err.println(e);
		}
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}

	private static Node[] readGraph(String verticesFilename, String edgesFilename) throws FileNotFoundException {
		// TODO Auto-generated method stub
		ArrayList<Node> nodeList = new ArrayList<Node>();
		try {
			BufferedReader fichier = new BufferedReader(new FileReader(verticesFilename));
			String str = null;
			
			while ((str = fichier.readLine()) != null)
			{	
				String[] parts = str.split(" ");
				Node v = new Node(Double.parseDouble(parts[0]), Double.parseDouble(parts[1]));
				nodeList.add(v);
			} 
			fichier.close();
		} catch (IOException e1) {
		      System.out.println("Erreur " + e1.getMessage()) ;
		      e1.printStackTrace() ;
		}
		
		try {
			BufferedReader fichier = new BufferedReader(new FileReader(edgesFilename));
			String str = null;
			while ((str = fichier.readLine()) != null)
			{
				
				String[] parts = str.split(" ");
				nodeList.get(Integer.parseInt(parts[0])).addNeighbor(nodeList.get(Integer.parseInt(parts[1])));
			}
			fichier.close();
		} catch (IOException e1) {
		      System.out.println("Erreur " + e1.getMessage()) ;
		      e1.printStackTrace() ;
		}
		
		return nodeList.toArray(new Node[nodeList.size()]);
	}

}