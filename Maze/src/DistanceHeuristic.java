
public interface DistanceHeuristic<Vertex> {

	public int distance(Vertex v0, Vertex v1);
}
